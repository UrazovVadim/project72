package university_;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class TestService {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testStringSerializeDeserialize() throws Exception {
        File file = folder.newFile("test.dat");
        ObjectOutputStream objectOutput = new ObjectOutputStream(new FileOutputStream(file));
        ObjectInputStream input = new ObjectInputStream(new FileInputStream(file));
        List<Flat> flats = new ArrayList<>();
        flats.add(new Flat());
        House house = new House("acds", "asdc",
                new Person("fsdf", "cac", "dc", new Date()), flats);

        Service.serializeHouse(objectOutput, house);
        House resultHouse = Service.deserializeHouse(input);
        assertTrue(resultHouse.equals(house));


        Service.serializeHouse(objectOutput, new House());
        House res = Service.deserializeHouse(input);
        assertTrue(res.equals(new House()));


    }

    @Test
    public void testStringSerializeDeserializeFile() throws Exception {
        House house = new House();

        String s = Service.serializeHouse(house);

        House resultHouse = Service.deserializeHouse(s);

        assertTrue(house.equals(resultHouse));

        Person p1 = new Person("Иван", "Иванов", "Иванович", new Date());
        Person p2 = new Person("Степан", "Иванов", "Иванович", new Date());
        Person p3 = new Person("Василий", "Петров", "Иванович", new Date());
        Person p4 = new Person("Людмила", "Петрова", "Ивановна", new Date());
        Person p5 = new Person("Игорь", "Семенов", "Иванович", new Date());

        List<Person> personList = new ArrayList<>();
        personList.add(p1);
        personList.add(p2);

        List<Person> personList1 = new ArrayList<>();
        personList1.add(p3);
        personList1.add(p4);

        List<Person> personList2 = new ArrayList<>();
        personList2.add(p5);


        List<Flat> fList = new ArrayList<>();
        Flat f1 = new Flat(1, "50.9", personList);
        Flat f2 = new Flat(2, "60.9", personList1);
        Flat f3 = new Flat(3, "40.9", personList2);
        fList.add(f1);
        fList.add(f2);
        fList.add(f3);

        House h2 = new House("123332212", "ul.Pushkina, 29",
                new Person("Иван", "Иванов", "Иванович", new Date()), fList);

        String out = Service.serializeHouse(h2);

        House h1 = Service.deserializeHouse(out);
        assertTrue(h1.equals(h2));

    }

    @Test(expected = Exception.class)
    public void testStringDeserializeFail() throws Exception {
        String s = null;
        Service.deserializeHouse(s);
    }
}
