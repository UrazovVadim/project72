package university_;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class InputOutputTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    public static File emptyDir;
    public static File binDir;


    public static File getFileMock(String fileName, String path) {
        File file = mock(File.class);

        when(file.isFile()).thenReturn(true);
        when(file.isDirectory()).thenReturn(false);
        when(file.getName()).thenReturn(fileName);
        when(file.getAbsolutePath()).thenReturn(path + fileName);

        return file;
    }

    public static File getDirMock(String dirName, String path, File... files) {
        File dir = mock(File.class);

        when(dir.isFile()).thenReturn(false);
        when(dir.isDirectory()).thenReturn(true);
        when(dir.getName()).thenReturn(dirName);
        when(dir.getAbsolutePath()).thenReturn(path + dirName);
        when(dir.listFiles()).thenReturn(files);

        return dir;
    }
//      ~/
//      ├ bin/
//      | └ file.txt
//      |  └ qwe.txt
    //     |  └ abc.txt
//      └


    @Test
    public void testFileReadWriteByteArray() {
        emptyDir = getDirMock("empty", "~/");
        File file = getFileMock("file.txt", "~/bin/");
        File file1 = getFileMock("qwe.txt", "~/bin/");
        File file2 = getFileMock("abc.txt", "~/bin/");
        binDir = getDirMock("bin", "~/", file, file1, file2);


        List<File> l = InputOutput.getListFile(emptyDir, null);
        List<File> l1 = InputOutput.getListFile(binDir, "txt");


        assertTrue(l.isEmpty());
        assertTrue(!l1.isEmpty());
        assertTrue(l1.contains(file));
        assertTrue(l1.contains(file1));
        assertTrue(l1.contains(file2));


    }

    @Test(expected = Exception.class)
    public void testFileReadWriteByteArrayFail() {
        emptyDir = getDirMock("empty", "~/");
        File file = getFileMock("file.txt", "~/bin/");
        File file1 = getFileMock("qwe.txt", "~/bin/");
        File file2 = getFileMock("abc.txt", "~/bin/");
        binDir = getDirMock("bin", "~/", file, file1, file2);


        InputOutput.getListFile(file, null);

    }
    @Test
    public void testReadAndWriteCharArray() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, 6};
        BufferedWriter out = new BufferedWriter(new FileWriter("test.txt"));
        InputOutput.inputArrayCharacterInStream(out, array);
        BufferedReader in = new BufferedReader(new FileReader("test.txt"));
        assertArrayEquals(array, InputOutput.outputArrayCharInStream(in, array));
    }


    @Test
    public void testFileReadWriteByteArray1() {
        int[] arrayToWrite = {0, 1, 5, 34, 67, 123};
        try {

            InputOutput.outputStreamArrayInt(arrayToWrite);

            int[] arrayRead = InputOutput.inputStreamArrayInt(arrayToWrite);
            assertArrayEquals(arrayToWrite, arrayRead);
        } catch (IOException ex) {
            fail();
        }
    }



    @Test
    public void testFileReadWriteByteArray3() throws IOException {
        File file = new File("test.dat");

        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            int[] arr = new int[10];
            int[] arr1 = new int[3];
            for (int i = 0; i < 10; i++)
                raf.writeInt(i);
            assertArrayEquals(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
                    InputOutput.inputRandStreamArray(raf, 0, arr));
            assertArrayEquals(new int[]{1, 2, 3},
                    InputOutput.inputRandStreamArray(raf, 4, arr1));
        }
    }

    @Test(expected = Exception.class)
    public void testFileReadWriteByteArrayFail3() throws Exception {
        File file = new File("test.dat");

        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {

            int[] arr1 = new int[3];
            for (int i = 0; i < 10; i++)
                raf.writeInt(i);

            InputOutput.inputRandStreamArray(raf, 500, arr1);
        }
    }
}
