package university_;

import java.io.Serializable;
import java.util.*;

public class House implements Serializable {
    private String numbHouse, address;
    private Person person;
    private List<Flat> list = new ArrayList<>();

    public House(String numbHouse, String address, Person person, List<Flat> list) {
        this.numbHouse = numbHouse;
        this.address = address;
        this.person = person;
        if (list != null) {
            for (Flat p : list) {
                if (p == null) {
                    throw new NullPointerException();
                }
            }

            this.list = list;
        } else {
            throw new NullPointerException();
        }
    }


    public House() {

    }

    public String getNumbHouse() {
        return numbHouse;
    }

    public void setNumbHouse(String numbHouse) {
        this.numbHouse = numbHouse;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<Flat> getList() {
        return list;
    }

    public void setList(List<Flat> list) {
        this.list = list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return Objects.equals(numbHouse, house.numbHouse) &&
                Objects.equals(address, house.address) &&
                Objects.equals(person, house.person) &&
                Objects.equals(list, house.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numbHouse, address, person, list);
    }
}