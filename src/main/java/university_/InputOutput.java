package university_;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class InputOutput {

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! Где мои комментарии? Я же уже проверял это задание.
     Где двоичный поток в который мы записываем данные? Как записать массив в файл?

    1. Записать массив целых чисел в двоичный поток. Прочитать массив целых чисел из двоичного потока.
    Предполагается, что до чтения массив уже создан, нужно прочитать n чисел, где n — длина массива.
    */
    public static void outputStreamArrayInt(int[] array) throws IOException {

        try (ByteArrayOutputStream f = new ByteArrayOutputStream()) {

            for (int i = 0; i < array.length; i++)
                f.write(array[i]);
        }
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! Где двоичиный поток из которого читаем в массив n? Как прочитать из файла данные?
    */
    public static int[] inputStreamArrayInt(int[] n) throws IOException {
        try (ByteArrayInputStream f = new ByteArrayInputStream(new byte[n.length])) {

            for (int i = 0; i < n.length; i++) {

                n[i] = f.read();
            }
            return n;
        }

    }


    public static void inputArrayCharacterInStream(Writer out, int[] array) throws IOException {
        PrintWriter prWr = new PrintWriter(out);
        for(int i = 0; i<array.length; i++){
            prWr.print(array[i]);
            prWr.print(' ');
        }
        /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
         Зачем здесь flush()?
        */
        out.flush();
    }

    public static int[] outputArrayCharInStream(Reader in, int[] array) throws IOException {
        Scanner scanner = new Scanner(in);
        for(int i = 0; i<array.length; i++){
            array[i] = scanner.nextInt();
        }
        return array;
    }

    public static int[] inputRandStreamArray(RandomAccessFile file, long offset, int[] array) throws IOException {
        /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
         Расскажите как создать массив с отрицательной длиной?
        */
        if (offset < 0 || array.length < 0)
            throw new IllegalArgumentException();

        file.seek(offset);


        for (int i = 0; i < array.length; i++)
            array[i] = file.readInt();

        return array;
    }

    public static List<File> getListFile(File dir, String extension) {
        if (!dir.isDirectory())
            throw new IllegalArgumentException();

        List<File> files = new ArrayList<>();

        /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
         Я третий раз уже читаю этот плохой код... (
        */
        if (extension != null) {
            if (extension.length() == 0) {
                for (File file : dir.listFiles()) {
                    if (file.isFile()) {
                        String fileName = file.getName();

                        if (fileName.lastIndexOf('.') == -1)
                            files.add(file);
                    }
                }
            } else {
                String suffix = "." + extension;

                for (File file : dir.listFiles()) {
                    if (file.isFile()) {
                        String fileName = file.getName();

                        if (fileName.endsWith(suffix))
                            files.add(file);
                    }
                }
            }
        }

        return files;
    }

}