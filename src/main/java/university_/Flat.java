package university_;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Flat implements Serializable {
    private int number;
    private String square;
    private List<Person> list = new ArrayList<>();

    public Flat(int number, String square, List<Person> list) {
        this.number = number;
        this.square = square;
        if (list != null) {
            for (Person p : list) {
                if (p == null) {
                    throw new NullPointerException();
                }
            }
            this.list = list;
        } else {
            throw new NullPointerException();
        }
    }

    public Flat() {

    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getSquare() {
        return square;
    }

    public void setSquare(String square) {
        this.square = square;
    }

    public List<Person> getList() {
        return list;
    }

    public void setList(List<Person> list) {
        this.list = list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return number == flat.number &&
                Objects.equals(square, flat.square) &&
                Objects.equals(list, flat.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, square, list);
    }
}
