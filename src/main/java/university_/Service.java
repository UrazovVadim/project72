package university_;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Service {
    public static String serializeHouse(ObjectOutputStream stream, House house) {
        try {
            stream.writeObject(house);
        } catch (IOException e) {
            /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
             Не обрабатывайте так исключения! Иначе ваш пользователь никогда не узнает, что
             у него отключили интернет или сгорел жесткий диск и поэтому его данные
             не сохраняются.
            */
            e.printStackTrace();
        }

        return stream.toString();
    }

    public static House deserializeHouse(ObjectInputStream stream) {

        List<Flat> d = new ArrayList<>();
        /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
         Почему вас не устроил new House()?
        */
        House house = new House("acds", "asdc",
                new Person("fsdf", "cac", "dc", new Date()), d);
        try {
            house = (House) stream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return house;
    }

    public static String serializeHouse(House house) throws Exception {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(house);
        return json;

    }

    public static House deserializeHouse(String json) throws Exception {
        ObjectMapper o = new ObjectMapper();
        return o.readValue(json, House.class);

    }
}

